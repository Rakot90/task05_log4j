package com.ivanytskyi;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "AC77e69085b52ce6a171fae5d94119d7a3";
    public static final String AUTH_TOKEN = "fc33eed0545902656baccb09bce4f688";
    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380964158385"), /*my phone number*/
                        new PhoneNumber("+14844585250"), str) .create(); /*attached to me number*/
    }
}