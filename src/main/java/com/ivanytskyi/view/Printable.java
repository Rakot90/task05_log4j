package com.ivanytskyi.view;

@FunctionalInterface
public interface Printable {
    void print();
}
