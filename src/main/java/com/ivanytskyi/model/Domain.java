package com.ivanytskyi.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Stream;

public class Domain {
    public int sumCost;
    public int sumWeight;
    public int x;  /**критерій прозорості який вводиться користувачем*/
    ArrayList<Jawelry> stones;
    ArrayList<Precious> stonesPrecious;
    ArrayList<SemiPrecious> stonesSemiPrecious;
    Stream<Jawelry> stonesStream;

    public Domain() {

/**створюємо всі наші дорогоцінності*/
        Precious ruby = new Precious("Ruby", 550, 4, 2);
        Precious diamond = new Precious("Diamond", 900, 3, 1);
        Precious alexandrite = new Precious("Alexandrite", 600, 5, 1);
        Precious emerald = new Precious("Emerald", 560, 4, 1);
        Precious sapphire = new Precious("Sapphire", 740, 7, 2);
        SemiPrecious opal = new SemiPrecious("Opal", 333, 3, 3);
        SemiPrecious pearl = new SemiPrecious("Pearl", 305, 7, 3);
        SemiPrecious garnet = new SemiPrecious("Garnet", 320, 10, 2);
        SemiPrecious lazurite = new SemiPrecious("Lazurite", 410, 8, 3);
        SemiPrecious coral = new SemiPrecious("Coral", 430, 15, 3);

/**добавляємо всі камні в лист массивів*/
        ArrayList<Jawelry> stones = new ArrayList<>();
        stones.add(diamond);
        stones.add(ruby);
        stones.add(alexandrite);
        stones.add(emerald);
        stones.add(sapphire);
        stones.add(opal);
        stones.add(pearl);
        stones.add(garnet);
        stones.add(lazurite);
        stones.add(coral);

        ArrayList<Precious> stonesPrecious = new ArrayList<>();
        stonesPrecious.add(diamond);
        stonesPrecious.add(ruby);
        stonesPrecious.add(alexandrite);
        stonesPrecious.add(emerald);
        stonesPrecious.add(sapphire);
        this.stonesPrecious = stonesPrecious;

        ArrayList<SemiPrecious> stonesSemiPrecious = new ArrayList<>();
        stonesSemiPrecious.add(opal);
        stonesSemiPrecious.add(pearl);
        stonesSemiPrecious.add(garnet);
        stonesSemiPrecious.add(lazurite);
        stonesSemiPrecious.add(coral);
        this.stonesSemiPrecious = stonesSemiPrecious;

/**створюємо компаратор для сортування за ціною каменів*/
        SortComparator stonesSort = new SortComparator();
        Collections.sort(stones, stonesSort);
        this.stones = stones;

/**стрім для підрахунку вартості та ваги всіх каменів*/
        int sumCost = stones.stream()
                .mapToInt(Jawelry::getCost)
                .sum();
        this.sumCost = sumCost;

        int sumWeight = stones.stream()
                .mapToInt(Jawelry::getWeight)
                .sum();
        this.sumWeight = sumWeight;
    }

    /**геттер який одразу фільтрує камені за критерієм*/
    public Stream<Jawelry> getStream(int x) {
        /**стрім для фільтрування каменів за потрібними критеріями*/
        Stream<Jawelry> stonesStream = stones.stream();
        stonesStream
                .filter(p -> p.getTransmittance() == x)
                .forEach(p -> System.out.println(p.getName()));
        this.x=x;
        this.stonesStream = stonesStream;
        return stonesStream;
    }

    public ArrayList<Jawelry> getArrayList() {
        return stones;
    }

    public ArrayList<Precious> getPreciousList() {
        return stonesPrecious;
    }

    public ArrayList<SemiPrecious> getSemiPreciousList() {
        return stonesSemiPrecious;
    }

    public Integer getSumCost() {
        return sumCost;
    }

    public Integer getSumWeight() {
        return sumWeight;
    }
}
