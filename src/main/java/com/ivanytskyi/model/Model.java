package com.ivanytskyi.model;

import java.util.ArrayList;
import java.util.stream.Stream;

public interface Model {
    Integer getSumCost();
    Integer getSumWeight();

    Stream<Jawelry> getStream(int x);
    ArrayList<Jawelry> getArrayList();
    ArrayList<Precious> getPreciousList();
    ArrayList<SemiPrecious> getSemiPreciousList();
}
