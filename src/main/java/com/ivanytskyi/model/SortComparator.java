package com.ivanytskyi.model;

import java.util.Comparator;

class SortComparator implements Comparator<Jawelry> {

    @Override
    public int compare(Jawelry a, Jawelry b) {
        return Integer.compare(a.cost, b.cost);
    }
}
