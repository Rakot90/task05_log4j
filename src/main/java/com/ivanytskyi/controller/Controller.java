package com.ivanytskyi.controller;

import com.ivanytskyi.model.Jawelry;
import com.ivanytskyi.model.Precious;
import com.ivanytskyi.model.SemiPrecious;
import java.util.ArrayList;
import java.util.stream.Stream;


public interface Controller {
    Integer getSumCost();
    Integer getSumWeight();

    Stream<Jawelry> getStream(int x);
    ArrayList<Jawelry> getArrayList();
    ArrayList<Precious> getPreciousList();
    ArrayList<SemiPrecious> getSemiPreciousList();
}
