package com.ivanytskyi.controller;

import com.ivanytskyi.model.*;
import java.util.ArrayList;
import java.util.stream.Stream;


public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new Logic();
    }

    @Override
    public Integer getSumCost() {
        return model.getSumCost();
    }

    @Override
    public Integer getSumWeight() {
        return model.getSumWeight();
    }

    @Override
    public Stream<Jawelry> getStream(int x) {
        return model.getStream(x);
    }

    @Override
    public ArrayList<Jawelry> getArrayList() {
        return model.getArrayList();
    }

    @Override
    public ArrayList<Precious> getPreciousList() {
        return model.getPreciousList();
    }

    @Override
    public ArrayList<SemiPrecious> getSemiPreciousList() {
        return model.getSemiPreciousList();
    }

}

